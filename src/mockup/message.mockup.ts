import { Message } from './../model/message.interface';

const messageList: Message[] = [
    {
        fromID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        fromName: 'A a',
        toID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        toName: 'B b',
        msg: 'Hello'
    },
    {
        fromID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        fromName: 'B b',
        toID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        toName: 'A a',
        msg: 'What\'s up?'
    },
    {
        fromID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        fromName: 'A a',
        toID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        toName: 'B b',
        msg: 'HW6?'
    },
    {
        fromID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        fromName: 'B b',
        toID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        toName: 'A a',
        msg: 'Done'
    },
    {
        fromID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        fromName: 'A a',
        toID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        toName: 'B b',
        msg: 'Too difficult'
    },
    {
        fromID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        fromName: 'B b',
        toID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        toName: 'A a',
        msg: 'Just follow the class'
    },
    {
        fromID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        fromName: 'A a',
        toID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        toName: 'B b',
        msg: 'Missed the class'
    },
    {
        fromID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        fromName: 'B b',
        toID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        toName: 'A a',
        msg: 'Shame on you'
    },
    {
        fromID: 'I60NvSsjtNY7TSMbKsDa62sHtFa2',
        fromName: 'A a',
        toID: 'DcjWRlGc1dSNIWF12zmTtMTsv4w2',
        toName: 'B b',
        msg: 'Shame on me'
    }
];

export const MESSAGE_LIST = messageList;