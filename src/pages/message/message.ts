import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Message } from './../../model/message.interface';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html'
})
export class MessagePage {

  msgList: Observable<any>;

  peeruid: string;
  peerName: string;
  myuid: string;
  myName: string;
  msg2send: string
  msgsub: Subscription;
  @ViewChild('content') ct: Content;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private afAuth: AngularFireAuth, private db: AngularFireDatabase) {
    this.peeruid = navParams.get('peeruid');
    this.peerName = navParams.get('peerName');
    this.myuid = this.afAuth.auth.currentUser.uid;
    this.myName = navParams.get('myName');
    console.log( this.peeruid +','+ this.peerName +','+ this.myuid +','+ this.myName);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagePage');
    this.msgList 
      = this.db.list(`/message-by-user/${this.myuid}/${this.peeruid}`)
            .valueChanges().pipe(map( changes => {
              changes.map( (mkey:{msgkey:string, msg:string, fromID:string})=> {
                this.db.object(`/messages/${mkey.msgkey}`).valueChanges().subscribe(
                  (x:Message)=> {
                    mkey.msg = x.msg;
                    mkey.fromID = x.fromID
                  })
              })
              return changes;
          }));


    this.msgsub = this.db.list(`message-by-user/${this.myuid}/${this.peeruid}`).valueChanges().subscribe(
      (x) => {
        this.db.object(`last-message/${this.myuid}/${this.peeruid}`).update({unread: 0});

        //if(this.ct) this.ct.scrollToBottom(0);
        if(this.ct) this.ct.scrollTo(0, 10000, 0);

      }
    );

    }
  
    ionViewDidEnter() {
      if(this.ct) this.ct.scrollToBottom(0);
    }

  async send() {
    let message = { fromID: this.myuid, toID: this.peeruid, msg: this.msg2send }

    let key = await this.db.list( 'messages' ).push( message).key;

    this.db.database.ref(`last-message/${this.peeruid}/${this.myuid}`)
      .transaction(function(msg) { 
          return {msgkey: key, unread: (msg ? msg.unread+1: 0)}; 
        }
    );
    this.db.object(`last-message/${this.myuid}/${this.peeruid}`).set(
        {msgkey: key, unread: 0}
    );

    await this.db.list(`message-by-user/${this.myuid}/${this.peeruid}`).push({msgkey: key});
    await this.db.list(`message-by-user/${this.peeruid}/${this.myuid}`).push({msgkey: key});

    
    //await this.db.object(`last-message/${this.myuid}/${this.peeruid}`).set({msgkey: key, name: this.myName});
    //await this.db.object(`last-message/${this.peeruid}/${this.myuid}`).set({msgkey: key, name: this.myName});

    this.msg2send = "";
  }

  ionViewWillUnload() {
    this.msgsub.unsubscribe();
  }
}
